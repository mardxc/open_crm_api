<?php
require_once 'models/bas_usuario.php';

class bas_usuarioController{
    private $connection;
    private $model;

    public function __construct(){
        $this->model = new BasUsuario();
    }
    
    public function readAll(){
        return $this->model->read();
    }
}
