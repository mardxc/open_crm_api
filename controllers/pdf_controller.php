<?php
require_once './vendor/autoload.php';
date_default_timezone_set("America/Mexico_City");
class pdfController
{
    private $mpdf;
    private $connection;
    private $model;
    public function __construct()
    {
        $this->mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'Letter']);
    }

    public function pdf_cotizacion($id)
    {
        require_once 'models/cli_cotizacion.php';
        $this->model = new CliCotizacion();

        $cotizacion = $this->model->pdf($id);

        $datos = $cotizacion['body']['datos'];
        $item = $cotizacion['body']['item'];

        $id_cotizacion = $id;

        $telefono_empresa = '487-143-0828';
        $direccion_empresa = 'Vicente Suarez #1, Las campanas';
        $direccion_empresa_ce = 'Queretaro, Queretaro';
        $cp_empresa = '76010';
        $nombre_empresa = 'BPFS PLANES DE NEGOCIOS Y ESTRATEGIAS TECNOLOGICAS';
        $logo_empresa = './public/img/logo_bpfs.jpeg';
        $fecha = date("d-m-Y");

        $nombre_cliente = $datos['nombre_cliente'];
        $direccion_cliente = '';
        //$direccion_cliente  = 'Melchor 15, Los Reyes, Corregidora, Queretaro';
        $cp_cliente = '';
        $telefono_cliente = '';
        $correo_cliente = $datos['correo_cliente'];
        $empresa_cliente = '';

        $body_tabla = '';

        $i = 1;
        $subtotal = 0;
        $iva = 0;
        $total = 0;
        foreach ($item as $fila) {
            $body_tabla .= '
                <tr data-iterate="item">
                    <td>' . $i . '</td>
                    <td>' . $fila['nombre_producto'] . ' ' . $fila['descripcion_producto'] . '</td>
                    <td>$' . $fila['precio_unidad'] . '</td>
                    <td>-' . $fila['descuento'] . '%</td>
                    <td>1 ' . $fila['unidad_medida'] . '</td>
                    <td>$' . $fila['importe_siniva'] . '</td>
                </tr>';
            $i++;
            $subtotal = $subtotal + $fila['importe_siniva'];
        }
        $iva = $subtotal * 0.16;
        $total = $subtotal + $iva;

        $html = $html = '<!DOCTYPE html>
        <html lang="es">
        <head>
            <meta charset="utf-8">
            <title>Cotización</title>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="./assets/factura_template/css/template.css">
        </head>
        <body>
            <div id="container">
                <section id="memo">
                    <div class="company-name">
                        <span>' . $nombre_empresa . '</span>
                        <div class="right-arrow"></div>
                    </div>
                    <div class="company-info">
                        <div>
                            <span>' . $direccion_empresa . '</span> <br>
                            <span>' . $direccion_empresa_ce . '</span>
                            <span>' . $cp_empresa . '</span>
                        </div>
                        <div>ventas@bpfs.com.mx</div>
                        <div>' . $telefono_empresa . '</div>
                    </div>
                </section>
                <section id="invoice-info">
                    <div>
                        <span>Fecha:</span>
                        <span>' . $fecha . '</span>
                    </div>
                </section>
                <section id="client-info">
                    <span>Atención a:</span>
                    <div>
                        <span class="bold">' . $nombre_cliente . '</span>
                    </div>
                    <div>
                        <span>' . $direccion_cliente . '</span>
                    </div>
                    <div>
                        <span>' . $cp_cliente . '</span>
                    </div>
                    <div>
                        <span>' . $telefono_cliente . '</span>
                    </div>
                    <div>
                        <span>' . $correo_cliente . '</span>
                    </div>
                    <div>
                        <span>' . $empresa_cliente . '</span>
                    </div>
                </section>
                <div class="clearfix"></div>
                <section id="invoice-title-number">
                    <span id="title">Cotización</span>
                    <span id="number">'.$id_cotizacion.'</span>
                </section>
                <section id="items">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <th>Item</th>
                            <th>Descripcion</th>
                            <th>Precio Unitario s/IVA</th>
                            <th>Descuento %</th>
                            <th>Unidad</th>
                            <th>Importe s/IVA</th>
                        </tr>
                        ' . $body_tabla . '
                    </table>
                </section>
                <div class="currency">
                    <span>*Todos los precios son en </span> <span>MXN</span>
                </div>
                <section id="sums">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <th>Subtotal</th>
                            <td>$' . $subtotal . '</td>
                        </tr>
                        <tr data-iterate="tax">
                            <th>IVA</th>
                            <td>$' . $iva . '</td>
                        </tr>
                        <tr class="amount-total">
                            <th style="color:white">Total</th>
                            <td style="color:white">$' . $total . '</td>
                        </tr>
                    </table>
                </section>
                <section id="terms">
                    <b>Datos bancarios</b>
                    <div>
                        <div>
                            <b>Beneficiario: </b>
                            <span>BPFS PLANES DE NEGOCIOS Y ESTRATEGIAS TECNOLOGICAS, S.A.S DE C.V.</span>
                        </div>
                        <div>
                            <b>Banco: </b>
                            <span>Banco Santander de Mexico S.A.</span>
                        </div>
                        <div>
                            <b>Número de cuenta: </b>
                            <span>65-50753838-1</span>
                        </div>
                        <div>
                            <b>Número de tarjeta: </b>
                            <span>5579 0890 0195 1998</span>
                        </div>
                        <div>
                            <b>CLABE: </b>
                            <span>0146-8065-5075-3838-14</span>
                        </div>
                        <div>
                            <b>Referencia: </b>
                            <span>Colocar el número de cotización o factura</span>
                        </div>
                        <div>
                            <b>Código SWIFT: </b>
                            <span>MBSXMXMMXXX</span>
                        </div>
                        <div>
                            <b>Enviar comprobante: </b>
                            <span style="color: red">ventas@bpfs.com.mx</span>
                        </div>
                    </div>
                </section>
            </div>
        </body>
        </html>';
        $this->mpdf->SetHTMLFooter(
            '<div class="payment-info">            
                <b style="color: red">Contacto urgente</b><br>
                <span>
                    <b>WhatsApp:</b> 442-785-4396
                </span> <br>
                <span>
                    <b>Atención a clientes:</b> 800 022 9888
                </span>
                
            </div>'
        );
        $this->mpdf->SetWatermarkImage($logo_empresa);
        $this->mpdf->showWatermarkImage = true;
        $this->mpdf->WriteHTML($html);

        $this->mpdf->Output();
    }
}
