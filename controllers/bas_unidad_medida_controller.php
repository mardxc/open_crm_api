<?php
require_once 'models/bas_unidad_medida.php';

class bas_unidadMedidaController
{
    private $connection;
    private $model;

    public function __construct()
    {
        $this->model = new BasUnidadMedida();
    }

    public function readAll()
    {
        return $this->model->read();
    }

    public function create($data)
    {
        return $this->model->create($data);
    }

    public function delete($id)
    {
        return $this->model->delete($id);
    }
}
