<?php
require_once 'models/cli_cotizacion.php';

class cli_cotizacionController
{
    private $connection;
    private $model;

    public function __construct()
    {
        $this->model = new CliCotizacion();
    }

    public function readAll()
    {
        return $this->model->read();
    }

    public function create($data)
    {
        return $this->model->create($data);
    }

    public function delete($id)
    {
        return $this->model->delete($id);
    }
}
