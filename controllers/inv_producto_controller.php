<?php
require_once 'models/inv_producto.php';

class inv_productoController
{
    private $connection;
    private $model;

    public function __construct()
    {
        $this->model = new InvProducto();
    }

    public function readAll()
    {
        return $this->model->read();
    }
    
    public function create($data)
    {
        return $this->model->create($data);
    }

    public function delete($id)
    {
        return $this->model->delete($id);
    }
}
