<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="public/css/style.css" />
    <title>Dcomuentación</title>
  </head>
  <body>
    <div class="container">
      <h1>Documentación API</h1>
      <div class="divbody">
        <h2>Empresa</h2>
        <p>Listar empresas [GET]</p>
        <code>/empresa <br>
          Retorna un array asociativo con claves <br>
          body[id_bas_empresa, nombre_corto, empresa, mail, tel_fijo, tel_movil, tipo_persona, estatus] o String ['La tabla esta vacia'] <br>
          status ['ok'] o ['err']
        </code>
        <p>Agregar empresa [POST]</p>
        <code>/empresa POST['data']</code>
      </div>
    </div>
  </body>
</html>
