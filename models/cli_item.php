<?php
require_once 'config/database.php';
class CliItem
{

    private $conn;
    public $respueta = array(
        "status" => '',
        "body" => '',
    );

    public function __construct()
    {
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    public function create($data)
    {
        try {
            $descuento = $data['descuento'];
            $cantidad = $data['cantidad'];
            $importe_siniva = $data['importe_siniva'];
            $id_producto = $data['id_producto'];
            $id_cotizacion = $data['id_cotizacion'];

            $query = 'INSERT INTO `cli_item` (
					`descuento`,
					`cantidad`,
					`importe_siniva`,
					`id_producto`,
					`id_cotizacion`)
				VALUES (
					:descuento,
					:cantidad,
					:importe_siniva,
					:id_producto,
					:id_cotizacion)';
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":descuento", $descuento, PDO::PARAM_INT);
            $statement->bindParam(":cantidad", $cantidad, PDO::PARAM_INT);
            $statement->bindParam(":importe_siniva", $importe_siniva, PDO::PARAM_STR);
            $statement->bindParam(":id_producto", $id_producto, PDO::PARAM_INT);
            $statement->bindParam(":id_cotizacion", $id_cotizacion, PDO::PARAM_INT);
            $statement->execute();

            $this->respueta['status'] = 'ok';
            $this->respueta['body'] = 'Cotización registrado';

        } catch (PDOException $e) {
            $this->respueta['status'] = 'err';
            $this->respueta['body'] = 'error: ' . $e->getMessage();
        }
        return $this->respueta;
    }

    public function update()
    {
    }

    public function delete()
    {
    }

    public function read()
    {
        try {
            $query = 'SELECT
				    ci.id_item,
				    ci.descuento,
				    ci.cantidad,
				    ci.importe_siniva,
				    ci.id_producto,
				    ci.id_cotizacion,
				    ip.nombre
				FROM
				    cli_item ci
				        INNER JOIN
				    inv_producto ip ON ci.id_producto = ip.id_producto';
            $statement = $this->connection->prepare($query);
            $statement->execute();

            $this->respueta['status'] = 'ok';
            if ($statement->rowCount() > 0) {
                $this->respueta['body'] = $statement->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $this->respueta['body'] = 'la tabla esta vacia';
            }

        } catch (PDOException $e) {
            $this->respueta['status'] = 'err';
            $this->respueta['body'] = 'error: ' . $e->getMessage();
        }
        return $this->respueta;
    }

    public function getparamstoUpdate($input)
    {
        $filterParams = [];
        foreach ($input as $param => $value) {
            $filterParams[] = "$param=:$param";
        }
        return implode(", ", $filterParams);
    }

    //Asociar todos los parametros a un sql
    public function bindAllValues($statement, $params)
    {
        foreach ($params as $param => $value) {
            $statement->bindValue(':' . $param, $value);
        }
        return $statement;
    }
}
?>