<?php
require_once 'config/database.php';
class CliProyecto
{

    private $conn;
    public $respueta = array(
        "status" => '',
        "body" => '',
    );

    public function __construct()
    {
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    public function create($data)
    {
        try {
            $nombre = $data['nombre'];
            $avance = $data['avance'];
            $estatus = $data['estatus'];

            $query = 'INSERT INTO `cli_proyecto` (
                    `nombre`,
                    `avance`,
                    `estatus`)
                VALUES (
                    :nombre,
                    :avance,
                    :estatus)';
            $statement = $this->connection->prepare($query);
            $statement->bindParam(":nombre", $nombre, PDO::PARAM_STR);
            $statement->bindParam(":avance", $avance, PDO::PARAM_STR);
            $statement->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            $statement->execute();

            $this->respueta['status'] = 'ok';
            $this->respueta['body'] = 'Proyecto registrado';

        } catch (PDOException $e) {
            $this->respueta['status'] = 'err';
            $this->respueta['body'] = 'error: ' . $e->getMessage();
        }
        return $this->respueta;
    }

    public function update()
    {
    }

    public function delete()
    {
    }

    public function read()
    {
        try {
            $query = 'SELECT
                    cp-id_proyecto,
                    cp.nombre,
                    cp.avance,
                    cp.estatus
                FROM
                    cli_proyecto cp';
            $statement = $this->connection->prepare($query);
            $statement->execute();

            $this->respueta['status'] = 'ok';
            if ($statement->rowCount() > 0) {
                $this->respueta['body'] = $statement->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $this->respueta['body'] = 'la tabla esta vacia';
            }

        } catch (PDOException $e) {
            $this->respueta['status'] = 'err';
            $this->respueta['body'] = 'error: ' . $e->getMessage();
        }
        return $this->respueta;
    }

    public function getparamstoUpdate($input)
    {
        $filterParams = [];
        foreach ($input as $param => $value) {
            $filterParams[] = "$param=:$param";
        }
        return implode(", ", $filterParams);
    }

    //Asociar todos los parametros a un sql
    public function bindAllValues($statement, $params)
    {
        foreach ($params as $param => $value) {
            $statement->bindValue(':' . $param, $value);
        }
        return $statement;
    }
}
