<?php
require_once 'config/database.php';
class CliCotizacion
{

    private $conn;
    public $respueta = array(
        "status" => '',
        "body" => '',
    );

    public function __construct()
    {
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    public function create($data)
    {
        try {
            $id_empresa = $data['id_empresa'];
            $mail = $data['mail'];
            $atencion = $data['atencion'];

            $item = $data['item'];

            $this->conn->beginTransaction();

            $query = 'INSERT INTO `cli_cotizacion` (
					`id_empresa`,
					`mail`,
                    `atencion`)
				VALUES (
					:id_empresa,
					:mail,
                    :atencion)';
            $statement = $this->conn->prepare($query);
            $statement->bindParam(":id_empresa", $id_empresa, PDO::PARAM_INT);
            $statement->bindParam(":mail", $mail, PDO::PARAM_STR);
            $statement->bindParam(":atencion", $atencion, PDO::PARAM_STR);
            $statement->execute();

            $id_cotizacion = $this->conn->lastInsertId();

            foreach ($item as $item) {
                $descuento = $item['descuento'];
                $cantidad = $item['cantidad'];
                $medida = $item['medida'];
                $importe_siniva = $item['importe'];
                $id_producto = $item['id_producto'];

                $query = 'INSERT INTO `cli_item` (
                    `descuento`,
                    `cantidad`,
                    `medida`,
                    `importe_siniva`,
                    `id_producto`,
                    `id_cotizacion`)
                VALUES (
                    :descuento,
                    :cantidad,
                    :medida,
                    :importe_siniva,
                    :id_producto,
                    :id_cotizacion)';
                $statement = $this->conn->prepare($query);
                $statement->bindParam(":descuento", $descuento, PDO::PARAM_STR);
                $statement->bindParam(":cantidad", $cantidad, PDO::PARAM_STR);
                $statement->bindParam(":medida", $medida, PDO::PARAM_STR);
                $statement->bindParam(":importe_siniva", $importe_siniva, PDO::PARAM_STR);
                $statement->bindParam(":id_producto", $id_producto, PDO::PARAM_INT);
                $statement->bindParam(":id_cotizacion", $id_cotizacion, PDO::PARAM_INT);
                $statement->execute();
            }

            $this->respueta['status'] = 'ok';
            $this->respueta['body'] = 'Cotización registrado';

            $this->conn->commit();

        } catch (PDOException $e) {
            $this->respueta['status'] = 'err';
            $this->respueta['body'] = 'error: ' . $e->getMessage();
            $this->conn->rollBack();
        }
        return $this->respueta;
    }

    public function pdf($id)
    {
        try {
            /*$query = "SELECT
            cc.id_cotizacion,
            ccl.empresa AS empresa_cliente,
            CONCAT(bd.calle,
            ' ',
            bd.num_ext,
            ' ',
            bd.colonia,
            ' ',
            bd.municipio,
            bd.estado) AS direccion_cliente,
            bd.cp AS cd_cliente,
            ccl.tel_fijo AS telefono_cliente,
            cc.mail AS correo_cliente,
            cc.atencion AS nombre_cliente
            FROM
            cli_cotizacion cc
            INNER JOIN
            cli_cliente ccl ON cc.id_empresa = ccl.id_bas_empresa
            INNER JOIN
            cli_direccion cd ON ccl.id_cliente = cd.id_cliente
            INNER JOIN
            bas_direcion bd ON cd.id_bas_direccion = bd.id_direccion
            WHERE
            cc.id_cotizacion = :id";*/
            $query = "SELECT
                    cc.id_cotizacion,
                    be.empresa AS empresa_cliente,
                    cc.mail AS correo_cliente,
                    cc.atencion AS nombre_cliente
                FROM
                    cli_cotizacion cc
                        INNER JOIN
                    bas_empresa be ON cc.id_empresa = be.id_bas_empresa
                    WHERE
                cc.id_cotizacion = :id";
            $statement = $this->conn->prepare($query);
            $statement->bindParam(":id", $id, PDO::PARAM_INT);
            $statement->execute();

            $datos = $statement->fetch(PDO::FETCH_ASSOC);

            $query = "SELECT
                    ci.descuento,
                    ci.cantidad,
                    ci.medida,
                    ci.importe_siniva,
                    ip.nombre AS nombre_producto,
                    ip.descripcion AS descripcion_producto,
                    ip.precio_unidad,
                    bum.descripcion AS unidad_medida
                FROM
                    cli_item ci
                    INNER JOIN
                inv_producto ip ON ci.id_producto = ip.id_producto
                    INNER JOIN
                bas_unidad_medida bum ON ip.id_unidad_medidia = bum.id_unidad_medidia
                WHERE
                    ci.id_cotizacion = :id";
            $statement = $this->conn->prepare($query);
            $statement->bindParam(":id", $id, PDO::PARAM_INT);
            $statement->execute();

            $item = $statement->fetchAll(PDO::FETCH_ASSOC);

            $this->respueta['status'] = 'ok';
            $this->respueta['body'] = [
                'datos' => $datos,
                'item' => $item,
            ];
        } catch (PDOException $e) {
            $this->respueta['status'] = 'err';
            $this->respueta['body'] = 'error: ' . $e->getMessage();
        }
        return $this->respueta;
    }

    public function update()
    {
    }

    public function delete()
    {
    }

    public function read()
    {
        try {
            $query = 'SELECT
                    cc.id_cotizacion,
                    be.empresa AS empresa_cliente,
                    cc.mail AS correo_cliente,
                    cc.atencion AS nombre_cliente,
                    (SELECT SUM(ci.importe_siniva) FROM cli_item ci WHERE ci.id_cotizacion = cc.id_cotizacion) AS importe_siniva
                FROM
                    cli_cotizacion cc
                        INNER JOIN
                    bas_empresa be ON cc.id_empresa = be.id_bas_empresa';
            $statement = $this->conn->prepare($query);
            $statement->execute();

            $this->respueta['status'] = 'ok';
            if ($statement->rowCount() > 0) {
                $this->respueta['body'] = $statement->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $this->respueta['body'] = 'la tabla esta vacia';
            }

        } catch (PDOException $e) {
            $this->respueta['status'] = 'err';
            $this->respueta['body'] = 'error: ' . $e->getMessage();
        }
        return $this->respueta;
    }

    public function getparamstoUpdate($input)
    {
        $filterParams = [];
        foreach ($input as $param => $value) {
            $filterParams[] = "$param=:$param";
        }
        return implode(", ", $filterParams);
    }

    //Asociar todos los parametros a un sql
    public function bindAllValues($statement, $params)
    {
        foreach ($params as $param => $value) {
            $statement->bindValue(':' . $param, $value);
        }
        return $statement;
    }
}
