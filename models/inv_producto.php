<?php
require_once 'config/database.php';
class InvProducto
{

    private $conn;
    public $respueta = array(
        "status" => '',
        "body" => '',
    );

    public function __construct()
    {
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    public function create($data)
    {
        try {
            $nombre             = $data['nombre'];
            $descripcion        = $data['descripcion'];
            $medida             = $data['medida'];
            $precio_unidad      = $data['precio_unidad'];
            $estatus            = $data['estatus'];
            $existencia         = $data['existencia'];
            $id_categoria       = $data['id_categoria'];
            $id_unidad_medidia  = $data['id_unidad_medidia'];

            $query = 'INSERT INTO inv_producto (
                    nombre,
                    descripcion,
                    medida,
                    precio_unidad,
                    estatus,
                    existencia,
                    id_categoria,
                    id_unidad_medidia)
                VALUES (
                    :nombre,
                    :descripcion,
                    :medida,
                    :precio_unidad,
                    :estatus,
                    :existencia,
                    :id_categoria,
                    :id_unidad_medidia)';
            $statement = $this->conn->prepare($query);
            
            $statement->bindParam(":nombre",                $nombre, PDO::PARAM_STR);
            $statement->bindParam(":descripcion",           $descripcion, PDO::PARAM_STR);
            $statement->bindParam(":medida",                $medida, PDO::PARAM_STR);
            $statement->bindParam(":precio_unidad",         $precio_unidad, PDO::PARAM_STR);
            $statement->bindParam(":estatus",               $estatus, PDO::PARAM_STR);
            $statement->bindParam(":existencia",            $existencia, PDO::PARAM_INT);
            $statement->bindParam(":id_categoria",          $id_categoria, PDO::PARAM_INT);
            $statement->bindParam(":id_unidad_medidia",     $id_unidad_medidia, PDO::PARAM_INT);
            $statement->execute();

            $this->respueta['status'] = 'ok';
            $this->respueta['body'] = 'Producto registrado';


        } catch (PDOException $e) {
            $this->respueta['status'] = 'err';
            $this->respueta['body'] = 'error: ' . $e->getMessage();
        }
        return $this->respueta;
    }

    public function update()
    {
    }

    public function delete()
    {
    }

    public function read()
    {
        try {
            $query = 'SELECT
				    ip.id_producto,
				    ip.nombre,
				    ip.descripcion,
				    ip.medida,
				    ip.precio_unidad,
				    ip.estatus,
				    ip.existencia,
				    bc.descripcion,
				    bum.descripcion as unidad_medida
				FROM
				    inv_producto ip
				        INNER JOIN
				    bas_categoria bc ON ip.id_categoria = bc.id_categoria
				        INNER JOIN
				    bas_unidad_medida bum ON ip.id_unidad_medidia = bum.id_unidad_medidia';
            $statement = $this->conn->prepare($query);
            $statement->execute();

            $this->respueta['status'] = 'ok';
            if ($statement->rowCount() > 0) {
                $this->respueta['body'] = $statement->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $this->respueta['body'] = 'la tabla esta vacia';
            }

        } catch (PDOException $e) {
            $this->respueta['status'] = 'err';
            $this->respueta['body'] = 'error: ' . $e->getMessage();
        }
        return $this->respueta;
    }

    public function getparamstoUpdate($input)
    {
        $filterParams = [];
        foreach ($input as $param => $value) {
            $filterParams[] = "$param=:$param";
        }
        return implode(", ", $filterParams);
    }

    //Asociar todos los parametros a un sql
    public function bindAllValues($statement, $params)
    {
        foreach ($params as $param => $value) {
            $statement->bindValue(':' . $param, $value);
        }
        return $statement;
    }
}
