<?php
header('Access-Control-Allow-Origin: *');
//header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header('Content-Type: application/json');
header('Access-Control-Allow-Credentials: true');

/** Enciende el reporteador de errores */
ini_set('display_errors', 1);
error_reporting(E_ALL);

/** Verifica si existe una URL via petición GET */
if (isset($_GET['url'])) {
    /// Asigna el valor de la URL de la petición
    $url = $_GET['url'];
    $response = '';

    /** Se lee el metodo solicitado [GET, POST, PUT, DELETE] */
    switch ($_SERVER['REQUEST_METHOD']) {
        case 'GET':{
                /// Obtiene el valor de id en la URL api.example.com/METODO/ID
                $id = intval(preg_replace('/[^0-9]+/', '', $url), 10);

                /// Obtiene el valor del metodo al que se dirige la petición
                switch ($url) {
                    case "categoria":{
                            /// Solicita el controlador correspondiente y el metodo
                            require_once 'controllers/bas_categoria_controller.php';
                            $controller = new bas_categoriaController();
                            $response = $controller->readAll();
                            /// Responde con un codigo 200 en caso de que la peticion sea correcta
                            http_response_code(200);
                            break;
                        }
                    case "categoria/$id":{
                            /// Solicita el controlador correspondiente y el metodo
                            require_once 'controllers/bas_categoria_controller.php';
                            $controller = new bas_categoriaController();
                            $response = $controller->readOne($id);
                            /// Responde con un codigo 200 en caso de que la peticion sea correcta
                            http_response_code(200);
                            break;
                        }
                    case "cotizacion":{
                            /// Solicita el controlador correspondiente y el metodo
                            require_once 'controllers/cli_cotizacion_controller.php';
                            $controller = new cli_cotizacionController();
                            $response = $controller->readAll();
                            /// Responde con un codigo 200 en caso de que la peticion sea correcta
                            http_response_code(200);
                            break;
                        }
                    case 'empresa':{
                            /// Solicita el controlador correspondiente y el metodo
                            require_once 'controllers/bas_empresa_controller.php';
                            $controller = new bas_empresaController();
                            $response = $controller->readAll();
                            /// Responde con un codigo 200 en caso de que la peticion sea correcta
                            http_response_code(200);
                            break;
                        }
                    case 'unidadMedida':{
                            /// Solicita el controlador correspondiente y el metodo
                            require_once 'controllers/bas_unidad_medida_controller.php';
                            $controller = new bas_unidadMedidaController();
                            $response = $controller->readAll();
                            /// Responde con un codigo 200 en caso de que la peticion sea correcta
                            http_response_code(200);
                            break;
                        }
                    case 'producto':{
                            /// Solicita el controlador correspondiente y el metodo
                            require_once 'controllers/inv_producto_controller.php';
                            $controller = new inv_productoController();
                            $response = $controller->readAll();
                            /// Responde con un codigo 200 en caso de que la peticion sea correcta
                            http_response_code(200);
                            break;

                        }
                    case 'item':{
                            /// Solicita el controlador correspondiente y el metodo
                            require_once 'controllers/cli_item_controller.php';
                            $controller = new cli_itemController();
                            $response = $controller->readAll();
                            /// Responde con un codigo 200 en caso de que la peticion sea correcta
                            http_response_code(200);
                            break;

                        }
                    case "pdf_cotizacion/$id":{
                            /// Solicita el controlador correspondiente y el metodo
                            require_once 'controllers/pdf_controller.php';
                            $controller = new pdfController();
                            $response = $controller->pdf_cotizacion($id);
                            /// Responde con un codigo 200 en caso de que la peticion sea correcta
                            http_response_code(200);
                            break;

                        }
                    default:{
                            break;
                        }
                }
                break;
            }
        case 'POST':{
                /// Obtiene el valor de DATA, en donde estan los campos y valores a usar
                $data = $_POST['data'];
                //$data = json_decode(file_get_contents("php://input"), true);
                /// Convierte lo obtenido en DATA a un JSON y verifica que no tenga errores, si tiene errores responde 400
                if (json_last_error() == 0) {
                    switch ($url) {
                        case 'categoria':{
                                /// Solicita el controlador correspondiente y el metodo
                                require_once 'controllers/bas_categoria_controller.php';
                                $controller = new bas_categoriaController();
                                $response = $controller->create($data);
                                /// Responde con un codigo 200 en caso de que la peticion sea correcta
                                http_response_code(200);
                                break;
                            }
                        case 'unidadMedida':{
                                /// Solicita el controlador correspondiente y el metodo
                                require_once 'controllers/bas_unidad_medida_controller.php';
                                $controller = new bas_unidadMedidaController();
                                $response = $controller->create($data);
                                /// Responde con un codigo 200 en caso de que la peticion sea correcta
                                http_response_code(200);
                                break;
                            }
                        case 'empresa':{
                                /// Solicita el controlador correspondiente y el metodo
                                require_once 'controllers/bas_empresa_controller.php';
                                $controller = new bas_empresaController();
                                $response = $controller->create($data);
                                /// Responde con un codigo 200 en caso de que la peticion sea correcta
                                http_response_code(200);
                                break;
                            }
                        case 'item':{
                                /// Solicita el controlador correspondiente y el metodo
                                require_once 'controllers/cli_item_controller.php';
                                $controller = new cli_itemController();
                                $response = $controller->create($data);
                                /// Responde con un codigo 200 en caso de que la peticion sea correcta
                                http_response_code(200);
                                break;
                            }
                        case 'producto':{
                                /// Solicita el controlador correspondiente y el metodo
                                require_once 'controllers/inv_producto_controller.php';
                                $controller = new inv_productoController();
                                $response = $controller->create($data);
                                /// Responde con un codigo 200 en caso de que la peticion sea correcta
                                http_response_code(200);
                                break;

                            }
                        case 'cotizacion':{
                                /// Solicita el controlador correspondiente y el metodo
                                require_once 'controllers/cli_cotizacion_controller.php';
                                $controller = new cli_cotizacionController();
                                $response = $controller->create($data);
                                /// Responde con un codigo 200 en caso de que la peticion sea correcta
                                http_response_code(200);
                                break;

                            }
                        default:{
                                break;
                            }
                    }
                } else {
                    http_response_code(400);
                }
                break;
            }

        case 'DELETE':{
                switch ($url) {
                    case "categoria/$id":{
                            /// Solicita el controlador correspondiente y el metodo
                            require_once 'controllers/bas_categoria_controller.php';
                            $controller = new bas_categoriaController();
                            $response = $controller->delete($id);
                            /// Responde con un codigo 200 en caso de que la peticion sea correcta
                            http_response_code(200);
                            break;
                        }
                    default:{
                            break;
                        }
                }
                break;
            }

        default:{
                break;
            }
    }
    echo json_encode($response);
} else {
    require_once "public/index.php";
}
